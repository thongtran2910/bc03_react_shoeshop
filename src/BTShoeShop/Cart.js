import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <td>ID</td>
            <td>Tên sản phẩm</td>
            <td>Giá</td>
            <td>Số lượng</td>
            <td>Thao tác</td>
          </thead>
          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td className="">
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSl(item.id, 1);
                      }}
                      className="btn btn-light border-info py-0 px-1"
                    >
                      <i className="fa fa-plus text-secondary" />
                    </button>
                    <span className="px-3">{item.soLuong}</span>
                    <button
                      onClick={() => {
                        this.props.handleTangGiamSl(item.id, -1);
                      }}
                      className="btn btn-light border-info py-0 px-1"
                    >
                      <i className="fa fa-minus text-secondary" />
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaSp(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
